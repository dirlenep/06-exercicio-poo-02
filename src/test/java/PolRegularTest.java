import static org.junit.Assert.*;

import org.junit.Test;

public class PolRegularTest {

	@Test
	public void testCalculoPerimetro() {
		PolRegular poligono = new PolRegular(5, 2);
		double area = poligono.calculoPerimetro();
		assertEquals(10.00, area, 0.001);
	}

	@Test
	public void testCalculoAnguloInterno() {
		PolRegular poligono = new PolRegular(5, 2);
		double area = poligono.calculoAnguloInterno();
		assertEquals(540.00, area, 0.001);
	}

	@Test
	public void testCalculoArea() {
		PolRegular poligono = new PolRegular(5, 2);
		double area = poligono.calculoArea();
		assertEquals(0.00, area, 0.001);
	}

}
