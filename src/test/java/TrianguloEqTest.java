import static org.junit.Assert.*;

import org.junit.Test;

public class TrianguloEqTest {

	@Test
	public void testCalculoAnguloInterno() {
		TrianguloEq triangulo = new TrianguloEq(5, 2);
		double area = triangulo.calculoAnguloInterno();
		assertEquals(60, area, 0.001);
	}

	@Test
	public void testCalculoArea() {
		TrianguloEq triangulo = new TrianguloEq(0, 10);
		double area = triangulo.calculoArea();
		assertEquals(43.301, area, 0.001);
	}
}
