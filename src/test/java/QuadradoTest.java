import static org.junit.Assert.*;

import org.junit.Test;

public class QuadradoTest {

	@Test
	public void testCalculoAnguloInterno() {
		Quadrado quadrado = new Quadrado(0, 10);
		double area = quadrado.calculoAnguloInterno();
		assertEquals(90.00, area, 0.001);
	}

	@Test
	public void testCalculoArea() {
		Quadrado quadrado = new Quadrado(0, 5);
		double area = quadrado.calculoArea();
		assertEquals(25.00, area, 0.001);
	}

}
