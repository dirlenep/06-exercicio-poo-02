import java.util.InputMismatchException;
import java.util.Scanner;

public class Tela {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		byte opcao = 0;
		int numeroLados = 0;
		int tamanhoLado = 0;
		
		try {
			System.out.println("Qual objeto quer criar?");
			System.out.println("Digite 1 para Pol�gono Regular");
			System.out.println("Digite 2 para Tri�ngulo Equil�tero");
			System.out.println("Digite 3 para Quadrado");
			System.out.println("Digite outro n�mero para sair");
			opcao = teclado.nextByte();
			switch (opcao) {
			case 1:
				System.out.println("Digite o n�mero de lados");
				numeroLados = teclado.nextInt();
				System.out.println("Digite o tamanho do lado");
				tamanhoLado = teclado.nextInt();
				PolRegular poligono = new PolRegular(numeroLados, tamanhoLado);
				System.out.println("O �ngulo interno �: " + poligono.calculoAnguloInterno());
				System.out.println("A �rea �: " + poligono.calculoArea());
				System.out.println("O per�metro �: " + poligono.calculoPerimetro());
				break;
			case 2:
				System.out.println("Digite o tamanho do lado");
				tamanhoLado = teclado.nextInt();
				numeroLados = 3;
				TrianguloEq triangulo = new TrianguloEq(numeroLados, tamanhoLado);
				System.out.println("O �ngulo interno �: " + triangulo.calculoAnguloInterno());
				System.out.println("A �rea �: " + triangulo.calculoArea());
				System.out.println("O per�metro �: " + triangulo.calculoPerimetro());
				break;
			case 3:
				System.out.println("Digite o tamanho do lado");
				tamanhoLado = teclado.nextInt();
				numeroLados = 4;
				Quadrado quadrado = new Quadrado(numeroLados, tamanhoLado);
				System.out.println("O �ngulo interno �: " + quadrado.calculoAnguloInterno());
				System.out.println("A �rea �: " + quadrado.calculoArea());
				System.out.println("O per�metro �: " + quadrado.calculoPerimetro());
				break;
			}
		} catch (InputMismatchException e) {
			System.out.println("Valor informado n�o � um n�mero");
		} catch (ArithmeticException e) {
			System.out.println("N�o � poss�vel divis�o por zero");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
