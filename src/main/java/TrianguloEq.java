
public class TrianguloEq extends PolRegular {

	/**
	 * @param numeroLados quantidade de lados do triangulo equilatero
	 * @param tamanhoLado tamanho dos lados do triangulo equilatero
	 */
	public TrianguloEq(int numeroLados, int tamanhoLado) {
		super(numeroLados, tamanhoLado);
	}

	/**
	 * Calculo da area do Triangulo Equilatero
	 */
	@Override
	public double calculoArea() {
		double resultArea = (Math.sqrt(3) * (getTamanhoLado() * getTamanhoLado())) / 4;
		return resultArea;
	}
	
	/**
	 * Padronização do angulo interno para 60°
	 */
	@Override
	public int calculoAnguloInterno() {
		return 60;
	}

}
