
public class PolRegular {
	private int numeroLados;
	private int tamanhoLado;
	/**
	 * 
	 * @param numeroLados numero de lados do poligono regular
	 * @param tamanhoLado tamanho do lado do poligono regular
	 */
	public PolRegular(int numeroLados, int tamanhoLado) {
		this.numeroLados = numeroLados;
		this.tamanhoLado = tamanhoLado;
	}
	
	public int getNumeroLados() {
		return numeroLados;
	}
	public void setNumeroLados(int numeroLados) {
		this.numeroLados = numeroLados;
	}
	public int getTamanhoLado() {
		return tamanhoLado;
	}
	public void setTamanhoLado(int tamanhoLado) {
		this.tamanhoLado = tamanhoLado;
	}
	
	/**
	 * 	calculo do perimetro do poligono regular
	 * @return retorna o perimetro
	 */
	public int calculoPerimetro() {
		int resultPerimetro = numeroLados * tamanhoLado;
		return resultPerimetro;
	}
	
	/**
	 * calculo do angulo interno do poligono regular
	 * @return retorna o angulo interno
	 */
	public int calculoAnguloInterno() {
		int resultAngulo = (numeroLados - 2) * 180;
		return resultAngulo;
	}
	
	/**
	 * calculo da area do poligono regular
	 * @return retorna 0 pois n�o � poss�vel calcular a area
	 */
	public double calculoArea() {
		return 0;
	}

}
