
public class Quadrado extends PolRegular {

	/**
	 * 
	 * @param numeroLados numero de lados do quadrado
	 * @param tamanhoLado tamanho dos lados do quadrado
	 */
	public Quadrado(int numeroLados, int tamanhoLado) {
		super(numeroLados, tamanhoLado);
	}
	
	/**
	 * Calculo de area do quadrado baseado nos lados
	 */
	@Override
	public double calculoArea() {
		double resultArea = getTamanhoLado() * getTamanhoLado();
		return resultArea;
	}
	
	/**
	 * Padronização do angulo interno para 90°
	 */
	@Override
	public int calculoAnguloInterno() {
		return 90;
	}

}
